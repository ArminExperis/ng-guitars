import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';

const { apiUsers, apiKey} = environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  //Dependency Injection.
  constructor(private readonly http: HttpClient) { }

  //Models, HttpClient, Observables, and RxJS Operators

  //
  public login(username:string): Observable<User> {
    return this.checkUsername(username)
    .pipe(
      switchMap((user: User | undefined) => {
        if(user === undefined){ // if user does not exist -> create user
          return this.createUser(username)
        } 
        return of(user) // otherwise return user
      })
    )
  }
  //Login


  //Check if user exists
  private checkUsername(username:string): Observable<User | undefined>{
    return this.http.get<User[]>(`${apiUsers}?username=${username}`)
    .pipe(
      map((response: User[]) => response.pop())
    )
  }


  // Create a user
private createUser(username: string): Observable<User>{
  const user = {
    username,
    favourites: []
  };

  const headers = new HttpHeaders({
    "Content-Type": "application/json",
    "x-api-key": apiKey
  })

  return this.http.post<User>(apiUsers, user, {
    headers
  })
}


  // IF User || Created User -> Store user
}
